class Model {
public:
	float Mat_I_1[46 * 9];
	float Mat_1_2[10 * 9];
	float Mat_2_3[10 * 2];

	__device__  float* Run(float input[46]) {
		float sum;
		// Output layer 1
		float outputL1[10];
		for (int i = 0; i < 9; i++)
		{
			sum = 0;
			for (int j = 0; j < 46; j++)
			{
				sum += input[j] * Mat_I_1[j * 9 + i];
			}
			outputL1[i] = tanh(0.5*sum);
		}
		outputL1[9] = 1;

		// Output layer 2
		float outputL2[10];
		for (int i = 0; i < 9; i++)
		{
			sum = 0;
			for (int j = 0; j < 10; j++)
			{
				sum += outputL1[j] * Mat_1_2[j * 9 + i];
			}
			outputL2[i] = tanh(0.5*sum);
		}
		outputL2[9] = 1;

		// Output layer 2
		float outputL3[2];
		for (int i = 0; i < 2; i++)
		{
			sum = 0;
			for (int j = 0; j < 10; j++)
			{
				sum += outputL2[j] * Mat_2_3[j * 2 + i];
			}
			outputL3[i] = 0.5*sum;
		}

		return outputL3;
	}
};