#pragma once

#include <stdio.h>
#include <chrono>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include "cinder/app/App.h"
#include "Material.cuh"
#include "Node.cuh"
#include "Particle.cuh"
#include "Model.cuh"


class SimulatorCUDA{
	int gSize, gSizeX, gSizeY, gSizeY_3;

	Node* grid;
	Node* d_grid;

	int* d_counter;

	std::vector<Node*> active;
	Node** d_active;

	int* d_activeCount;

	Material materials[numMaterials];
	Material* d_materials;

	dim3 dimBlockP;
	dim3 dimGridP;

	dim3 dimBlockP2;
	dim3 dimGridP2;

	dim3 dimBlockN;
	dim3 dimGridN;

	dim3 dimBlockA;
	dim3 dimGridA;

	dim3 dimBlockP5_1;
	dim3 dimGridP5_1;

	dim3 dimBlockP5_3;
	dim3 dimGridP5_3;

	float uscip(float p00, float x00, float y00, float p01, float x01, float y01, float p10, float x10, float y10, float p11, float x11, float y11, float u, float v);
public:
	Model* d_model;
	std::vector<Particle> particles;
	Particle* d_particles;
	ParticleInput* d_particlesInput;
	ParticleOutputL1* d_particlesOutputL1;
	ParticleOutputL2* d_particlesOutputL2;
	ParticleOutputL3* d_particlesOutputL3;
	int particleCount;
	struct cudaGraphicsResource *cudaVboResource;

	int step;
	int maxStep;

	std::chrono::high_resolution_clock::time_point begin;
	std::chrono::high_resolution_clock::time_point end;

	float scale;
	int blockDim;

	SimulatorCUDA();
	void loadModel(char* path);
	void initializeGrid(int sizeX, int sizeY);
	void addParticles(int n);
	void initializeCUDA(int blockDim);
	void update();
};