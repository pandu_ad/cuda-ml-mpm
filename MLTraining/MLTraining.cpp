#include <fann.h>
#include <fann_cpp.h>
#include <floatfann.h>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

void shuffleArray(int* array, int size)
{
	int n = size;
	while (n > 1)
	{
		// 0 <= k < n.
		int k = rand() % n;

		// n is now the last pertinent index;
		n--;

		// swap array[n] with array[k]
		int temp = array[n];
		array[n] = array[k];
		array[k] = temp;
	}
}

int* kfold(int size, int k)
{
	int* indices = new int[size];

	for (int i = 0; i < size; i++)
		indices[i] = i % k;

	srand(100);
	shuffleArray(indices, size);

	return indices;
}

//int main(int argc, char** argv)
//{
//	const float desired_error = (const float) 0.35;
//	const unsigned int max_epochs = 1000;
//	const unsigned int epochs_between_reports = 100;
//
//	int* indices = kfold(100000, 5);
//
//	fann* annList[4];
//	float mseList[4];
//
//	// Save test set
//	cout << "Writing testSet \n";
//
//	ifstream myReadFile;
//	myReadFile.open("../MPM/Data.txt");
//
//	ofstream testSet;
//	testSet.open("testSet.txt");
//	testSet << "20000 125 2\n";
//
//	int line = 0;
//	string STRING;
//	getline(myReadFile, STRING); // Saves the line in STRING.
//	while (line < 100000 && !myReadFile.eof()) // To get you all the lines.
//	{
//		int kValue = indices[line];
//
//		if (kValue == 0) {
//			getline(myReadFile, STRING);
//			testSet << STRING + "\n";
//			getline(myReadFile, STRING);
//			testSet << STRING + "\n";
//		}
//		else {
//			getline(myReadFile, STRING);
//			getline(myReadFile, STRING);
//		}
//		line++;
//	}
//
//	myReadFile.close();
//	testSet.close();
//
//	cout << "Writing testSet DONE \n\n";
//
//	for (int i = 1; i < 5; i++) {
//		cout << "Training k-" + to_string(i) + "\n";
//
//		ofstream dataSet;
//		ofstream validationSet;
//
//		dataSet.open("dataSet.txt");
//		validationSet.open("validationSet.txt");
//
//		dataSet << "60000 125 2\n";
//		validationSet << "20000 125 2\n";
//
//		ifstream myReadFile;
//		myReadFile.open("../MPM/Data.txt");
//
//		int line = 0;
//		string STRING;
//		getline(myReadFile, STRING); // Saves the line in STRING.
//		while (line < 100000 && !myReadFile.eof()) // To get you all the lines.
//		{
//			int kValue = indices[line];
//
//			if (kValue != 0) {
//				if (kValue == i) {
//					getline(myReadFile, STRING);
//					validationSet << STRING + "\n";
//					getline(myReadFile, STRING);
//					validationSet << STRING + "\n";
//				}
//				else {
//					getline(myReadFile, STRING);
//					dataSet << STRING + "\n";
//					getline(myReadFile, STRING);
//					dataSet << STRING + "\n";
//				}
//			}
//			else {
//				getline(myReadFile, STRING);
//				getline(myReadFile, STRING);
//			}
//			line++;
//		}
//
//		myReadFile.close();
//		dataSet.close();
//		validationSet.close();
//
//		// Train ann
//		struct fann *ann = fann_create_standard(4, 125, 75, 25, 2);
//
//		fann_set_activation_function_hidden(ann, FANN_SIGMOID_SYMMETRIC);
//		fann_set_activation_function_output(ann, FANN_LINEAR);
//		fann_set_training_algorithm(ann, FANN_TRAIN_RPROP);
//
//		struct fann_train_data * data = NULL;
//		data = fann_read_train_from_file("dataSet.txt");
//
//		fann_set_scaling_params(
//			ann,
//			data,
//			-1,	/* New input minimum */
//			1,	/* New input maximum */
//			-1,	/* New output minimum */
//			1);	/* New output maximum */
//
//		fann_scale_train(ann, data);
//		fann_set_train_error_function(ann, FANN_ERRORFUNC_LINEAR);
//		fann_set_learning_rate(ann, 0.3);
//		fann_set_learning_momentum(ann, 0.5);
//
//		fann_train_on_data(ann, data, max_epochs, epochs_between_reports, desired_error);
//		fann_save(ann, ("MPM" + to_string(i) + ".net").c_str());
//		fann_destroy_train(data);
//
//		// Calculate MSE
//		struct fann_train_data * validateSet = NULL;
//		validateSet = fann_read_train_from_file("validationSet.txt");
//		annList[i - 1] = ann; 
//		mseList[i - 1] = fann_test_data(ann, validateSet);
//		fann_destroy_train(validateSet);
//
//		cout << "Training k-" + to_string(i) + " DONE, mse: " + to_string(mseList[i - 1]) + "\n\n";
//	}
//
//	// Get the best ann
//	float min = 999;
//	int bestIdx = 0;
//	for (int i = 0; i < 4; i++) {
//		if (mseList[i] < min) {
//			min = mseList[i];
//			bestIdx = i;
//		}
//	}
//
//	for (int i = 0; i < 4; i++) {
//		cout << "Model: " + to_string(i) + ", mse: " + to_string(mseList[i]) + "\n";
//	}
//	cout << "Chosen model: " + to_string(bestIdx) + ", with mse: " + to_string(mseList[bestIdx]) + "\n";
//
//	struct fann *ann = annList[bestIdx];
//	fann_save(ann, "MPM.net");
//
//	fann_destroy(ann);
//	for (int i = 0; i < 4; i++) {
//		if(i != bestIdx){
//			fann_destroy(annList[i]);
//		}
//	}
//
//	return 0;
//}

const unsigned int max_epochs = 2250;

void setWeight(fann *annX) {
	//Print weight
	unsigned int connum;
	connum = fann_get_total_connections(annX);

	struct fann_connection *con;
	con = (fann_connection *)calloc(connum, sizeof(*con));

	/* Get weight matrix */
	fann_get_connection_array(annX, con);

	/* Print weight matrix */
	float r;
	float HI = 0.1;
	float LO = -0.1;
	srand(1);
	for (int i = 0; i < connum; ++i) {
		r = LO + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (HI - LO)));
		fann_set_weight(annX, con[i].from_neuron,
			con[i].to_neuron, r);
	}
}

void printWeight(fann *annX) {
	//Print weight
	unsigned int connum;
	connum = fann_get_total_connections(annX);

	struct fann_connection *con;
	con = (fann_connection *)calloc(connum, sizeof(*con));

	/* Get weight matrix */
	fann_get_connection_array(annX, con);

	/* Print weight matrix */
	for (int i = 0; i < connum; ++i) {
		printf("weight from %u to %u: %f\n", con[i].from_neuron,
			con[i].to_neuron, con[i].weight);
	}
}

void train() {
	remove("MPM.net");

	cout << "Training, max epoch: " + to_string(max_epochs) + '\n';

	// Train ann
	struct fann *annX = fann_create_standard(4, 45, 9, 9, 2);

	fann_set_activation_function_hidden(annX, FANN_SIGMOID_SYMMETRIC);
	fann_set_activation_function_output(annX, FANN_LINEAR);
	fann_set_training_algorithm(annX, FANN_TRAIN_RPROP);

	struct fann_train_data * dataTrainX = NULL;
	dataTrainX = fann_read_train_from_file("../MPM/Data.txt");
	cout << "Training data: " + to_string(dataTrainX->num_data) + '\n';

	setWeight(annX);
	printWeight(annX);

	int epochCounter = 0;
	float trainingMse;
	float validateMse;
	while (epochCounter < max_epochs) {
		epochCounter++;

		fann_reset_MSE(annX);
		fann_train_epoch(annX, dataTrainX);

		fann_reset_MSE(annX);
		trainingMse = fann_test_data(annX, dataTrainX);
		cout << "epoch " + to_string(epochCounter) + ", Training mse: " + to_string(trainingMse) + "\n";

		fann_save(annX, "MPM.net");
	}
	fann_destroy_train(dataTrainX);

	cout << "Training DONE";
}

int main(int argc, char** argv)
{
	train();

	return 0;
}
